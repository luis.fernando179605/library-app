<?php
    $server="localhost";
    $user_db="root";
    $password_db="";
    $db = "libreria";

    $user = "";
    if($_POST["usuario"]!=null){
        $user = $_POST["usuario"];
    }


    $sqlcon = mysqli_connect($server,$user_db,$password_db,$db);

    if(!$sqlcon){
        echo "Error: No se puede conectar a la base de datos MySQL".PHP_EOL;
    }

    $result0 = mysqli_query($sqlcon,"SELECT * FROM usuario WHERE usuario = '$user'");
    $row0 = mysqli_fetch_array($result0);
    $user_id = $row0["id"];
    $queryF = "SELECT * FROM prestamo WHERE id_usuario = '$user_id'";
    $result = mysqli_query($sqlcon,$queryF);
    session_start();
    $_SESSION['pre_res']= $queryF;
?>
<!DOCTYPE html>
    <html>
        <head>
            <title >Prestamos</title>
            <link rel="stylesheet" href="styles.css">
        </head>
        <h2 class="title">Listado de prestamos </h2>
        <form action="prestamos_persona_html.php"method="post"> 
            <input type= "text" placeholder= "Nombre de usuario" name = "usuario">
            <input type= "submit" value="Enter" class="button"><br>
        </form>
        <body>
            <?php
            if (mysqli_num_rows($result) > 0) {
                ?>
                <div class="container" >
                    <table class="table table-hover">
                    <thead>
                    <tr>
                        <td scope="col" >Titulo</td>
                        <td scope="col" >Fecha inicial</td>
                        <td scope="col" >Fecha final</td>
                        <td scope="col" >Multa</td>
                        <td scope="col" > scope="col" Dias restantes</td>
                        <td scope="col" >Estado</td> 
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i=0;
                        while($row = mysqli_fetch_array($result)) {
                            $fId = $row["id_libro"];
                            $fRes = mysqli_query($sqlcon,"SELECT * FROM libro WHERE id = '$fId'");
                            $fRow = mysqli_fetch_array($fRes);
                            $title = $fRow["titulo"];
                        ?>
                            <tr>
                                <th class="tdata"><?php echo $title; ?></th>
                                <td class="tdata"><?php echo $row["fecha_ini"]; ?></td>
                                <td class="tdata"><?php echo $row["fecha_fin"]; ?></td>
                                <td class="tdata"><?php echo $row["multa"]; ?></td>
                                <td class="tdata"><?php echo $row["dias_res"]; ?></td>
                                <td class="tdata"><?php echo $row["estado"]; ?></td>
                            </tr>
                    <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                    </table>
                </div>    
                <?php
            }
else{
    echo "<h2 class=\"text-center\"> No se encontraron prestamos </h2>";
}
?>
            <div class="container">
                <div class="col"></div>
                <div class="col">
                <div class="col">

                <br><a  role="button" class="btn btn-lg btn-block btn-danger" href="/library-app/menu_admin.html">Salir</a>
                <br>
                <br>
                <form action="/library-app/Reportes/reporte_libros_pdf.php" method="post"> 
                    <div>
                    <input type="submit" class="btn btn-lg btn-block btn-secondary" value="Imprimir"><br><br>    
                    </div>
                </form>
                </div>
                </div>
                <div class="col"></div>
            </div>
        </body>
    </html>