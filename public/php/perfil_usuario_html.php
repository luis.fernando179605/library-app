<?php
    $server="localhost";
    $user_db="root";
    $password_db="";
    $db = "libreria";

    session_start();
    $user = $_SESSION["user_b"];
    $today = date('Y-m-d H:i:s');

    $sqlcon = mysqli_connect($server,$user_db,$password_db,$db);

    if(!$sqlcon){
        echo "Error: No se puede conectar a la base de datos MySQL".PHP_EOL;
    }

    $result0 = mysqli_query($sqlcon,"SELECT * FROM usuario WHERE usuario = '$user'");
    $row0 = mysqli_fetch_array($result0);
    $user_id = $row0["id"];
    $result = mysqli_query($sqlcon,"SELECT * FROM prestamo WHERE id_usuario = '$user_id'");
    
?>
<!DOCTYPE html>
    <html>
        <head>
            <title >Prestamos</title>
            <link rel="stylesheet" href="styles.css">
            <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, minimum-scale=1.0">
            <link rel="stylesheet" href="forms.css">
            <script src="https://code.jquery.com/jquery-3.4.1.js"
                integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
                integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>
        </head>
        <h2 class="title text-center">Listado de prestamos  </h2>
        <body>
            <?php
            if (mysqli_num_rows($result) > 0) {
                ?>
                <div class="container" > 
                    <table class="table table-hover">
                    <thead>
                    <tr>
                        <td scope="col">Titulo</td>
                        <td scope="col">Fecha inicial</td>
                        <td scope="col">Fecha final</td>
                        <td scope="col">Multa</td>
                        <td scope="col">Dias restantes</td>
                        <td scope="col">Estado</td> 
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i=0;
                        while($row = mysqli_fetch_array($result)) {
                            $fId = $row["id_libro"];
                            $fRes = mysqli_query($sqlcon,"SELECT * FROM libro WHERE id = '$fId'");
                            $fRow = mysqli_fetch_array($fRes);
                            $title = $fRow["titulo"];
                        ?>
                            <tr>
                                <th scope="row" class="tdata"><?php echo $title; ?></th>
                                <td class="tdata"><?php echo $row["fecha_ini"]; ?></td>
                                <td class="tdata"><?php echo $row["fecha_fin"]; ?></td>
                                <td class="tdata"><?php echo $row["multa"]; ?></td>
                                <td class="tdata"><?php echo $row["dias_res"]; ?></td>
                                <td class="tdata"><?php echo $row["estado"]; ?></td>
                            </tr>
                    <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                    </table>
                </div>    
                <?php
            }
else{
    echo "No se encontraron prestamos";
}
?>
    <div class="container">
                <div class="col"></div>
                <div class="col">
                <div class="col">

                <br><a  role="button" class="btn btn-lg btn-block btn-danger" href="/library-app/php/menu_usuario.html">Salir</a>
                <br>
                <br>
                </div>
                </div>
                <div class="col"></div>
            </div>
        </body>
    </html>