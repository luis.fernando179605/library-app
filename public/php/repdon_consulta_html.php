<?php
    $server="localhost";
    $user_db="root";
    $password_db="";
    $db = "libreria";

    $sqlcon = mysqli_connect($server,$user_db,$password_db,$db);

    if(!$sqlcon){
        echo "Error: No se puede conectar a la base de datos MySQL".PHP_EOL;
    }

    if($_POST["tipo"]==null){
        $queryF = "SELECT * FROM repdon ";
        $result = mysqli_query($sqlcon,$queryF);
        session_start();
        $_SESSION['don_res']= $queryF;
    }

    else{
        $stat = $_POST["tipo"];
        $queryF = "SELECT * FROM repdon WHERE tipo = '$stat'";
        $result = mysqli_query($sqlcon,$queryF);
        session_start();
        $_SESSION['don_res']= $queryF;
    }
?>

<!DOCTYPE html>
    <html>
        <head>
            <title >Donaciones/Reposiciones</title>
            <link rel="stylesheet" href="styles.css">
            <meta name="viewport"
            content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, minimum-scale=1.0">
            <link rel="stylesheet" href="forms.css">
            <script src="https://code.jquery.com/jquery-3.4.1.js"
                integrity="sha256-WpOohJOqMqqyKL9FccASB9O0KwACQJpFTUBLTYOVvVU=" crossorigin="anonymous"></script>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
                integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
                integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
                crossorigin="anonymous"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
                integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
                crossorigin="anonymous"></script>
        </head>
        <h2 class="title display-3 text-center">Listado de Donaciones/Reposiciones </h2>
        <form action="repdon_consulta_html.php"method="post"> 
            <select id="tipo" name="tipo" placeholder="Tipo de transaccion" class="form-control" required>
                                 <option value ="donado">Donacion</option>
                                 <option value="donacion">Donado</option>
                                 <option value="reposicion">Reposicionado</option>
                          </select>
            <input type= "submit" value="Enter" class="btn btn-primary btn-lg btn-block "><br>
        </form>
        <body>
            <?php
            if (mysqli_num_rows($result) > 0) {
                ?>
                <div class="container">
                    <table class="table table-hover">
                    <thead>
                    <tr>
                        <td scope="col">Libro</td>
                        <td scope="col">Usuario</td>
                        <td scope="col">Fecha</td>
                        <td scope="col">Tipo</td>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i=0;
                        while($row = mysqli_fetch_array($result)) {
                            $fId = $row["id_libro"];
                            $fRes = mysqli_query($sqlcon,"SELECT * FROM libro WHERE id = '$fId'");
                            $fRow = mysqli_fetch_array($fRes);
                            $title = $fRow["titulo"];
                            
                            $fuId = $row["id_usuario"];
                            $fResu = mysqli_query($sqlcon,"SELECT * FROM usuario WHERE id = '$fuId'");
                            $Rowu = mysqli_fetch_array($fResu);
                            $uname = $Rowu["nombre"];
                        ?>
                            <tr>
                                <th class="tdata"><?php echo $title; ?></th>
                                <td class="tdata"><?php echo $uname; ?></td>
                                <td class="tdata"><?php echo $row["fecha"]; ?></td>
                                <td class="tdata"><?php echo $row["tipo"]; ?></td>
                            </tr>
                    <?php
                        $i++;
                    }
                    ?>
                    </tbody>
                    </table>
                </div>    
                <?php
            }
else{
    echo "No se encontraron prestamos";
}
?>
<div class="container">
    <div class="col"></div>
    <div class="col">
    <div class="col">

    <br><a  role="button" class="btn btn-lg btn-block btn-danger" href="/library-app/menu_admin.html">Salir</a>
    <br>
    <br>
    <form action="/library-app/Reportes/reporte_libros_pdf.php" method="post"> 
        <div>
        <input type="submit" class="btn btn-lg btn-block btn-secondary" value="Imprimir"><br><br>    
        </div>
    </form>
    </div>
    </div>
    <div class="col"></div>
</div>
</body>
</html>