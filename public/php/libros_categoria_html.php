<?php
    $server="localhost";
    $user_db="root";
    $password_db="";
    $db = "libreria";
    $sqlcon = mysqli_connect($server,$user_db,$password_db,$db);

    if($_POST["asignatura"]==null){
        $query = "SELECT * FROM libro ";
        session_start();
        $_SESSION['lib_res']= $query;
    }
    else{
        $assign = $_POST["asignatura"];
        $query = "SELECT * FROM libro WHERE asignatura = '$assign'";
        session_start();
        $_SESSION['lib_res']= $query;
    }
    $result = mysqli_query($sqlcon,$query);
    
?>

<!DOCTYPE html>
    <html>
        <head>
            <title >Libros</title>
            <link rel="stylesheet" href="styles.css">
        </head>
        <form action="libros_categoria_html.php"method="post"> 
            <h2 class="title">Categoria</h2>
            <select id="asignatura" name="asignatura" placeholder="Asignatura del Libro" required>
                                <!--Si quieren agregar mas asignaturas, asegurense de que el value siempre sea un valor alftabetico, en mayusculas, de 3 caracteres-->
                                 <option value =" "></option>
                                 <option value="Psicologia">Psicologia</option>
                                 <option value="Biologia">Biologia</option>
                                 <option value="Matematicas">Matematicas</option>
                                 <option value="Computacion">Computacion</option>
                                 <option value="Ingenieria">Ingenieria</option>
                          </select>
            <input type= "submit" value="Enter" class="button"><br>
        </form>
        <h2 class="title">Listado de libros  </h2>
        <body>
            <?php
            if (mysqli_num_rows($result) > 0) {
                ?>
                <div>
                    <table>
                    <tr>
                        <td>Id</td>
                        <td>Titulo</td>
                        <td>Editorial</td>
                        <td>Edicion</td>
                        <td>Autores</td>
                        <td>Id de ubicacion</td>
                        <td>Volumen</td>
                        <td>Asignatura</td>
                        <td>Estado</td>
                    </tr>
                    <?php
                        $i=0;
                        while($row = mysqli_fetch_array($result)) {
                        ?>
                            <tr>
                                <td class="tdata"><?php echo $row["id"]; ?></td>
                                <td class="tdata"><?php echo $row["titulo"]; ?></td>
                                <td class="tdata"><?php echo $row["editorial"]; ?></td>
                                <td class="tdata"><?php echo $row["edicion"]; ?></td>
                                <td class="tdata"><?php echo $row["autores"]; ?></td>
                                <td class="tdata"><?php echo $row["id_ubicacion"]; ?></td>
                                <td class="tdata"><?php echo $row["volumen"]; ?></td>
                                <td class="tdata"><?php echo $row["asignatura"]; ?></td>
                                <td class="tdata"><?php echo $row["estado"]; ?></td>
                            </tr>
                    <?php
                        $i++;
                    }
                    ?>
                    </table>
                </div>    
                <?php
            }
else{
    echo "No se encontraron libros";
}
?>
        <a href="/library-app/menu_admin.html">Salir</a><br>
        <form action="/library-app/Reportes/reporte_libros_pdf.php" method="post"> 
            <div>
            <input type= "submit" value="Imprimir"><br><br>    
            </div>
        </form>
        </body>