<?php
require "fpdf.php";

$servidor="localhost";
$usuario="root";
$password="";
$db_name="libreria";

$connection=mysqli_connect($servidor,$usuario,$password,$db_name);
//esta clase es necesaria
class myPDF extends FPDF{
    //el header, podriamos agregar un logo si queremos
    function header(){
        $this->SetFont('Arial','B',14);
        $this->Cell(0,5,'Reporte Test',0,0,'C');
        $this->Ln(20);
    }
     //lo de abajo, ahorita muestra no. de pagina nada mas
    function footer(){
        $this->SetY(-15);
        $this->SetFont('Arial','',14);
        $this->Cell(500,10,'Pagina '.$this->PageNo().'/{nb}',0,0,'C');
    }
    //Los titulos de columnas de la tabla
    function headerTable(){
        $this->SetFont('Arial','B',12);
        $this->Cell(25,10,'ID',1,0,'C');
        $this->Cell(30,10,'Titulo',1,0,'C');
        $this->Cell(35,10,'Autor/es',1,0,'C');
        $this->Cell(25,10,'Editorial',1,0,'C');
        $this->Cell(20,10,'Edicion',1,0,'C');
        $this->Cell(23,10,'Volumen',1,0,'C');
        $this->Cell(35,10,'Ubicacion',1,0,'C');
        $this->Cell(35,10,'Asignatura',1,0,'C');
        $this->Cell(25,10,'Estado',1,0,'C');
        $this->Ln();
    }
    //lo que saca los datos de sql y los mete a la tabla
    function viewTable($connection){
        $this->SetFont('Arial','',11);
        //el query que saca los datos que quieres. nada mas modifica el select como lo necesites
        $result=mysqli_query($connection,"SELECT * FROM libro");
        while($rows=mysqli_fetch_assoc($result)){
            $this->Cell(25,10,$rows["id"],1,0,'L');
            $this->Cell(30,10,$rows["titulo"],1,0,'L');
            $this->Cell(35,10,$rows["autores"],1,0,'L');
            $this->Cell(25,10,$rows["editorial"],1,0,'L');
            $this->Cell(20,10,$rows["edicion"],1,0,'L');
            $this->Cell(23,10,$rows["volumen"],1,0,'L');
            $this->Cell(35,10,$rows["id_ubicacion"],1,0,'L');
            $this->Cell(35,10,$rows["asignatura"],1,0,'L');
            $this->Cell(25,10,$rows["estado"],1,0,'L');
            $this->Ln();
        }
    }
}
//Esto es sagrado, no lo toques
$pdf=new myPDF();
$pdf->AliasNbPages();
$pdf->AddPage('L','A4',0);
$pdf->headerTable();
$pdf->viewTable($connection);
$pdf->Output();

?>