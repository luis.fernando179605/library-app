<?php
$servidor = "localhost";
$usuario = "root";
$password = "";
$db_name = "libreria";
$connection = mysqli_connect($servidor, $usuario, $password, $db_name);


$idLib = $_POST["idU"];
session_start();
$user = $_SESSION["user_b"];
$result1 = mysqli_query($connection, "SELECT * FROM usuario WHERE usuario = '$user' AND solicitud > 0");
$rownum1 = mysqli_num_rows($result1);
$result2 = mysqli_query($connection, "SELECT * FROM libro WHERE id = '$idLib' AND estado = 'disponible'");
$rownum2 = mysqli_num_rows($result2);
if ($rownum1 > 0 && $rownum2 > 0) {
    mysqli_query($connection, "UPDATE libro SET estado = 'solicitado' WHERE id = '$idLib'");
    mysqli_query($connection, "UPDATE usuario SET solicitud = 0 WHERE usuario = '$user'");
    echo '<script language="javascript">alert("Libro apartado con exito");window.location.href="../menu_usuario.html"</script>';
} else {
    echo '<script language="javascript">alert("Ya tienes una transaccion en curso");window.location.href="../menu_usuario.html"</script>';
}
?>

<!doctype HTML>

<html>

<head>
    <title>Solicitud Biblioteca UAA</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, minimum-scale=1.0">
    <link rel="stylesheet" href="forms.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

</head>

<body>
    <div class="container">
        <form action="solicitud_html.php" method="post" class="libraryForm">
            <div class="formAltas">
                <ul>
                    <h2 class="display-4">Solicitar</h2>
                    <li>
                        <label for="id_libro">ID del libro</label>
                        <input type="text" id="idU" name="idU" placeholder="ID del libro">
                    </li>
                </ul>
                <button class="button" type="submit" value="alta">Solicitud</button>
                <button class="button" type="reset" value="regresar">Borrar Datos</button>
                <button class="button"><a href="/library-app/menu_usuario.html" class="option">Regresar</a></button>
            </div>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>

</html>