-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2019 at 10:08 PM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `libreria`
--

-- --------------------------------------------------------

--
-- Table structure for table `libro`
--

CREATE TABLE `libro` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `editorial` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `edicion` int(11) NOT NULL,
  `autores` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_ubicacion` int(11) NOT NULL,
  `volumen` int(11) NOT NULL,
  `asignatura` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `libro`
--

INSERT INTO `libro` (`id`, `titulo`, `editorial`, `edicion`, `autores`, `id_ubicacion`, `volumen`, `asignatura`, `estado`) VALUES
('BIO1027', 'Biologia Marina II', 'Water Dudes', 1, 'Ryan Fisherman', 102, 1, 'Biologia', 'solicitado'),
('COM1041', 'Pokemon Red', 'Nintendo', 1, 'Reggie Fils Aime', 104, 1, 'Computacion', 'no disponible'),
('COM10410', 'Informatica 102', 'UAA', 1, 'Jose Gustavo', 104, 3, 'Computacion', 'disponible'),
('COM1042', 'Pokemon Red', 'Nintendo', 1, 'Reggie Fils Aime', 104, 1, 'Computacion', 'disponible'),
('COM1043', 'Pokemon Blue', 'Nintendo', 1, 'Reggie Fils Aime', 104, 1, 'Computacion', 'disponible'),
('COM1046', 'Pokemon Yello', 'Nintendo', 1, 'Reggie Fils Aime', 104, 1, 'Computacion', 'disponible'),
('COM1049', 'Informatica 101', 'UAA', 1, 'Jose Gustavo', 104, 2, 'Computacion', 'disponible'),
('MAT1035', 'Algebra de Baldor', 'Al Baktur', 1, 'Baldor', 103, 1, 'Matematicas', 'no disponible'),
('PSI1014', 'La biblia', 'Jesus', 1, 'God Himself', 101, 1, 'Psicologia', 'eliminado'),
('PSI1018', 'Donado', 'Donado', 0, 'Donado', 101, 0, 'Psicologia', 'eliminado');

-- --------------------------------------------------------

--
-- Table structure for table `prestamo`
--

CREATE TABLE `prestamo` (
  `id` int(11) NOT NULL,
  `id_libro` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `fecha_ini` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `multa` float NOT NULL,
  `dias_res` int(11) NOT NULL,
  `estado` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `prestamo`
--

INSERT INTO `prestamo` (`id`, `id_libro`, `id_usuario`, `fecha_ini`, `fecha_fin`, `multa`, `dias_res`, `estado`) VALUES
(4, 'COM1041', 3, '2020-02-13', '2020-02-16', 0, 3, 'terminado'),
(5, 'BIO1027', 3, '2019-12-20', '2019-12-23', 0, 3, 'terminado'),
(7, 'MAT1035', 3, '2019-12-31', '2020-01-03', 0, 3, 'terminado'),
(8, 'MAT1035', 3, '2019-12-31', '2020-01-03', 0, 3, 'activo'),
(9, 'COM1041', 3, '2019-12-20', '2019-12-23', 0, 3, 'activo');

-- --------------------------------------------------------

--
-- Table structure for table `repdon`
--

CREATE TABLE `repdon` (
  `id` int(11) NOT NULL,
  `id_libro` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  `fecha` date NOT NULL,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `repdon`
--

INSERT INTO `repdon` (`id`, `id_libro`, `id_usuario`, `fecha`, `tipo`) VALUES
(8, 'COM1046', 0, '2019-12-19', 'Reposicion'),
(9, 'PSI1018', 0, '2019-12-20', 'Donacion'),
(10, 'PSI1018', 0, '2019-12-31', 'Donado'),
(11, 'BIO1027', 0, '2019-12-21', 'Reposicion'),
(12, 'MAT1035', 0, '2019-12-20', 'Donado'),
(13, 'COM1046', 0, '2019-12-20', 'Reposicion'),
(15, 'COM1046', 3, '2019-12-20', 'Reposicion'),
(16, 'COM1046', 3, '2019-12-20', 'Reposicion'),
(17, 'COM1042', 0, '2019-12-20', 'Donado'),
(19, 'COM10410', 6, '2019-12-20', 'Donacion'),
(20, 'PSI1018', 0, '2019-12-19', 'Donado'),
(21, '6', 0, '2019-12-20', 'Reposicion'),
(23, 'BIO1027', 6, '2019-12-20', 'Reposicion'),
(25, 'BIO1027', 6, '2019-12-20', 'Reposicion'),
(26, 'BIO1027', 6, '2019-12-20', 'Reposicion');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `usuario` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rol` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `a_paterno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `a_materno` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_nac` date NOT NULL,
  `direccion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `solicitud` int(11) NOT NULL,
  `extension` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id`, `usuario`, `password`, `rol`, `nombre`, `a_paterno`, `a_materno`, `fecha_nac`, `direccion`, `solicitud`, `extension`) VALUES
(1, 'admin', 'system', 'admin', 'test_admin', 'test_surname1', 'test_surname2', '1996-09-28', 'test_address', 0, 0),
(3, 'Buzz', '12345', 'user', 'Test', 'Tes', 'Test', '2019-12-20', '', 0, 0),
(6, 'Irving', '12345', 'user', 'Irving Alejandro', 'Medina', 'Muñoz', '2019-12-20', 'Rivero de la Hacienda 338 0 Casablanca 20294', 1, 1),
(7, 'miguelortiz', '123456', 'admin', 'Miguel Angel', 'Ortiz', 'Esparza', '2000-10-17', 'Av. VillaPlata 303, Coto Toledo Int. 41 338 69 Posobravo 20908', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `libro`
--
ALTER TABLE `libro`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prestamo`
--
ALTER TABLE `prestamo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `repdon`
--
ALTER TABLE `repdon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `prestamo`
--
ALTER TABLE `prestamo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `repdon`
--
ALTER TABLE `repdon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
